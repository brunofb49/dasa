package com.dasa.utils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.dasa.domain.DadoPopulacional;

public class DatasetReaderTest {

    private DatasetReader reader;

    @Before
    public void init() {
        reader = createDatasetReader();
    }

    @Test
    public void readsDataset() {

        final List<DadoPopulacional> dadosDadoPopulacionalList = reader.readDataset();
        assertThat(dadosDadoPopulacionalList, hasSize(17));
    }

    private DatasetReader createDatasetReader() {
        try {
            final Path path = Paths.get("src/test/resources/datasets", "dados_populacionais.csv");
            final Reader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"));
            return new DatasetReader(reader);
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
package com.dasa.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.dasa.domain.DadoPopulacional;

@Data
@EqualsAndHashCode(callSuper = true)
public class EstatisticaAnoCrescimentoResponse extends EstatisticaAnoResponse {
    private Double crescimento;

    public EstatisticaAnoCrescimentoResponse(final DadoPopulacional pop, final Double crescimento) {
        super(pop);
        this.crescimento = crescimento;
    }

}

package com.dasa.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

import com.dasa.domain.ParticipacaoEvento;
import com.dasa.enuns.CampanhaEnum;
import com.dasa.enuns.SexoEnum;

@Data
public class ParticipacaoEventoDTO {

    private Long id;

    @NotNull(message = "propriedade ano nao pode ser nula")
    private String ano;

    @NotNull(message = "propriedade sexo nao pode ser nula")
    private SexoEnum sexo;

    @NotNull(message = "propriedade campanha nao pode ser nula")
    private CampanhaEnum campanha;

    public ParticipacaoEventoDTO() {
        super();
    }

    public ParticipacaoEventoDTO(final ParticipacaoEvento participacaoEvento) {
        super();
        id = participacaoEvento.getId();
        ano = participacaoEvento.getAno();
        sexo = participacaoEvento.getSexo();
        campanha = participacaoEvento.getCampanha();
    }

}

package com.dasa.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ParticipacaoEventoProporcaoResponse {

    private String ano;
    private BigDecimal percentualHomensMama;
    private BigDecimal percentualMulheresMama;
    private BigDecimal percentualHomensProstata;
    private BigDecimal percentualMulheresProstata;

    public ParticipacaoEventoProporcaoResponse(final String ano, final BigDecimal percentualHomensMama, final BigDecimal percentualHomensProstata) {
        super();
        this.ano = ano;
        this.percentualHomensMama = percentualHomensMama;
        if (percentualHomensMama != null) {
            percentualMulheresMama = new BigDecimal(100).subtract(percentualHomensMama);
        }
        this.percentualHomensProstata = percentualHomensProstata;
        if (percentualHomensProstata != null) {
            percentualMulheresProstata = new BigDecimal(100).subtract(percentualHomensProstata);
        }
    }
}

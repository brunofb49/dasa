package com.dasa.dto;

import static com.dasa.utils.MathUtils.calcPorcentagem;

import java.math.BigDecimal;

import lombok.Data;

import com.dasa.domain.DadoPopulacional;

@Data
public class EstatisticaAnoResponse {
    private String ano;
    private BigDecimal populacaoTotal;
    private BigDecimal percentualHomens;
    private BigDecimal percentualMulheres;

    public EstatisticaAnoResponse(final DadoPopulacional pop) {
        ano = pop.getAno();
        populacaoTotal = pop.getPopulacaoTotal();
        percentualHomens = calcPorcentagem(new BigDecimal(pop.getTotalHomens()), pop.getPopulacaoTotal());
        percentualMulheres = calcPorcentagem(new BigDecimal(pop.getTotalMulheres()), pop.getPopulacaoTotal());
    }

}

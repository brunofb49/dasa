package com.dasa.controllers;

import java.util.Optional;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dasa.domain.DadoPopulacional;
import com.dasa.dto.EstatisticaAnoCrescimentoResponse;
import com.dasa.dto.EstatisticaAnoResponse;
import com.dasa.service.DadosPopulacionaisService;
import com.dasa.utils.MathUtils;

@RestController
@RequestMapping("dadosPopulacionais")
public class DadosPopulacionaisController {

    @Autowired
    private DadosPopulacionaisService service;

    @RequestMapping(value = "/2010", method = RequestMethod.GET)
    public EstatisticaAnoResponse get2010data() throws NotFoundException {
        return new EstatisticaAnoResponse(service.obterPopulacaoPorAno(Optional.of("2010")));
    }

    @RequestMapping(value = "/2017", method = RequestMethod.GET)
    public EstatisticaAnoResponse get2017data() throws NotFoundException {
        final DadoPopulacional dadoPopulacionalInicial = service.obterPopulacaoPorAno(Optional.of("2010"));
        final DadoPopulacional dadoPopulacional = service.obterPopulacaoPorAno(Optional.of("2016"));
        final Double crescimento = MathUtils.calcEstimativaDeCrescimento(dadoPopulacionalInicial.getPopulacaoTotal().doubleValue(), dadoPopulacional.getPopulacaoTotal().doubleValue(),
                Integer.valueOf(dadoPopulacional.getAno()) - Integer.valueOf(dadoPopulacionalInicial.getAno()));
        final EstatisticaAnoCrescimentoResponse estatisticaAnoCrescimentoResponse = new EstatisticaAnoCrescimentoResponse(dadoPopulacional, crescimento);
        return estatisticaAnoCrescimentoResponse;
    }

}

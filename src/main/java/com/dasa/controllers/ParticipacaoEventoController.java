package com.dasa.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dasa.dto.ParticipacaoEventoDTO;
import com.dasa.dto.ParticipacaoEventoProporcaoResponse;
import com.dasa.enuns.CampanhaEnum;
import com.dasa.service.ParticipacaoEventoService;

@RestController
@RequestMapping("participacaoEvento")
public class ParticipacaoEventoController {

    @Autowired
    private ParticipacaoEventoService service;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ParticipacaoEventoDTO criar(@Valid @RequestBody final ParticipacaoEventoDTO dto) {
        return service.criar(dto);
    }

    @RequestMapping(value = "{ano}/mama", method = RequestMethod.GET)
    public List<ParticipacaoEventoDTO> getMama(@PathVariable("ano") final String ano) {
        return service.findByAnoAndCampanha(ano, CampanhaEnum.CANCER_MAMA);
    }

    @RequestMapping(value = "{ano}/prostata", method = RequestMethod.GET)
    public List<ParticipacaoEventoDTO> getProstata(@PathVariable("ano") final String ano) {
        return service.findByAnoAndCampanha(ano, CampanhaEnum.CANCER_PROSTATA);
    }

    @RequestMapping(value = "{ano}/proporcao", method = RequestMethod.GET)
    public ParticipacaoEventoProporcaoResponse getProporcao(@PathVariable("ano") final String ano) {
        return service.getProporcao(ano);
    }
}

package com.dasa.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.dasa.domain.ParticipacaoEvento;
import com.dasa.enuns.CampanhaEnum;
import com.dasa.enuns.SexoEnum;

@Transactional
public interface ParticipacaoEventoRepository extends CrudRepository<ParticipacaoEvento, Long> {

    List<ParticipacaoEvento> findByAnoAndCampanha(final String ano, final CampanhaEnum campanha);

    Long countByAnoAndCampanhaAndSexo(final String ano, final CampanhaEnum campanha, final SexoEnum sexo);

}

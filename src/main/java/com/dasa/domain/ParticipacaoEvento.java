package com.dasa.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

import com.dasa.enuns.CampanhaEnum;
import com.dasa.enuns.SexoEnum;

@Data
@Entity
@Table(name = "participacao_evento")
public class ParticipacaoEvento implements Serializable {

    private static final long serialVersionUID = -633120466571861359L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String ano;

    private SexoEnum sexo;

    private CampanhaEnum campanha;

    public ParticipacaoEvento() {
        super();
    }

    public ParticipacaoEvento(final Long id, final String ano, final SexoEnum sexo, final CampanhaEnum campanha) {
        super();
        this.id = id;
        this.ano = ano;
        this.sexo = sexo;
        this.campanha = campanha;
    }

}

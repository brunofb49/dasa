package com.dasa.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "dados_populacionais")
public class DadoPopulacional implements Serializable {

    private static final long serialVersionUID = 2847829202890774920L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String ano;

    private BigDecimal populacaoTotal;

    private Long totalHomens;

    private Long totalMulheres;

    public DadoPopulacional() {
        super();
    }

    public DadoPopulacional(final String ano, final String totalPopulacao, final String totalHomens, final String totalMulheres) {
        this.ano = ano;
        populacaoTotal = BigDecimal.valueOf(Long.valueOf(totalPopulacao));
        this.totalHomens = Long.valueOf(totalHomens);
        this.totalMulheres = Long.valueOf(totalMulheres);
    }

}

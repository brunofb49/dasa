package com.dasa.service;

import java.util.Optional;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dasa.domain.DadoPopulacional;
import com.dasa.repository.DadosPopulacionaisRepository;

@Service
public class DadosPopulacionaisServiceimpl implements DadosPopulacionaisService {

    @Autowired
    private DadosPopulacionaisRepository dadosPopulacionaisRepository;

    @Override
    public DadoPopulacional obterPopulacaoPorAno(final Optional<String> ano) throws NotFoundException {

        final String anoCenso = ano.get();

        if (!ano.isPresent()) {
            throw new IllegalArgumentException("Parametro Ano é Obrigatório");
        }

        final DadoPopulacional dadoPopulacional = dadosPopulacionaisRepository.findByAno(anoCenso);

        if (dadoPopulacional == null) {
            throw new NotFoundException("Não possui informação para o ano " + anoCenso);
        }

        return dadoPopulacional;
    }
}

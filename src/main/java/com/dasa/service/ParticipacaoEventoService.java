package com.dasa.service;

import java.util.List;

import com.dasa.dto.ParticipacaoEventoDTO;
import com.dasa.dto.ParticipacaoEventoProporcaoResponse;
import com.dasa.enuns.CampanhaEnum;

public interface ParticipacaoEventoService {

    ParticipacaoEventoDTO criar(final ParticipacaoEventoDTO dto);

    List<ParticipacaoEventoDTO> findByAnoAndCampanha(final String ano, final CampanhaEnum campanha);

    ParticipacaoEventoProporcaoResponse getProporcao(final String ano);

}

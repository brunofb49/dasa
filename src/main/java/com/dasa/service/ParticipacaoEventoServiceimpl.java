package com.dasa.service;

import static com.dasa.utils.MathUtils.calcPorcentagem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dasa.domain.ParticipacaoEvento;
import com.dasa.dto.ParticipacaoEventoDTO;
import com.dasa.dto.ParticipacaoEventoProporcaoResponse;
import com.dasa.enuns.CampanhaEnum;
import com.dasa.enuns.SexoEnum;
import com.dasa.repository.ParticipacaoEventoRepository;

@Service
public class ParticipacaoEventoServiceimpl implements ParticipacaoEventoService {

    @Autowired
    private ParticipacaoEventoRepository participacaoEventoRepository;

    @Override
    public ParticipacaoEventoDTO criar(final ParticipacaoEventoDTO dto) {
        return new ParticipacaoEventoDTO(participacaoEventoRepository.save(new ParticipacaoEvento(dto.getId(), dto.getAno(), dto.getSexo(), dto.getCampanha())));
    }

    @Override
    public List<ParticipacaoEventoDTO> findByAnoAndCampanha(final String ano, final CampanhaEnum campanha) {
        return convertByListDTO(participacaoEventoRepository.findByAnoAndCampanha(ano, campanha));
    }

    private List<ParticipacaoEventoDTO> convertByListDTO(final List<ParticipacaoEvento> participacoesEventos) {
        final List<ParticipacaoEventoDTO> participacoesEventosDTO = new ArrayList<ParticipacaoEventoDTO>();

        participacoesEventos.forEach(p -> {
            participacoesEventosDTO.add(new ParticipacaoEventoDTO(p));
        });

        return participacoesEventosDTO;
    }

    @Override
    public ParticipacaoEventoProporcaoResponse getProporcao(final String ano) {
        final BigDecimal totalHomemMama = new BigDecimal(participacaoEventoRepository.countByAnoAndCampanhaAndSexo(ano, CampanhaEnum.CANCER_MAMA, SexoEnum.MASCULINO));
        final BigDecimal totalMulherMama = new BigDecimal(participacaoEventoRepository.countByAnoAndCampanhaAndSexo(ano, CampanhaEnum.CANCER_MAMA, SexoEnum.FEMININO));
        final BigDecimal totalHomemProstata = new BigDecimal(participacaoEventoRepository.countByAnoAndCampanhaAndSexo(ano, CampanhaEnum.CANCER_PROSTATA, SexoEnum.MASCULINO));
        final BigDecimal totalMulherProstata = new BigDecimal(participacaoEventoRepository.countByAnoAndCampanhaAndSexo(ano, CampanhaEnum.CANCER_PROSTATA, SexoEnum.FEMININO));

        return new ParticipacaoEventoProporcaoResponse(ano, calcPorcentagem(totalHomemMama, totalHomemMama.add(totalMulherMama)), calcPorcentagem(totalHomemProstata, totalHomemProstata.add(totalMulherProstata)));
    }

}

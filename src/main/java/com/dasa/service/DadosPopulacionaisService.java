package com.dasa.service;

import java.util.Optional;

import javassist.NotFoundException;

import com.dasa.domain.DadoPopulacional;

public interface DadosPopulacionaisService {

    DadoPopulacional obterPopulacaoPorAno(final Optional<String> ano) throws NotFoundException;

}

package com.dasa.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathUtils {

    public static BigDecimal calcPorcentagem(final BigDecimal valorParcial, final BigDecimal valorTotal) {
        if (valorTotal.equals(new BigDecimal(0))) {
            return null;
        }
        return valorParcial.multiply(new BigDecimal(100)).divide(valorTotal, 2, RoundingMode.HALF_UP);
    }

    public static Double calcEstimativaDeCrescimento(final Double populacaoInicial, final Double populacaoFinal, final Integer anosPeriodo) {
        return Math.pow(populacaoFinal / populacaoInicial, 1.0 / anosPeriodo) - 1;
    }
}
